# nerdtree-open_in_system
A VIM / NERDTree plugin. Opens directories or files using the system default file handler


Plugin for [NERDTree](https://github.com/scrooloose/nerdtree) that opens files or directories using the system defaults.

## Installation

Copy the file open_in_system.vim to the nerdtree instaliation. For example, if using pathogen, copy the file here: ~/.vim/bundle/nerdtree/nerdtree_plugin/.

## Usage
When navigating in NERDTree, select file or directory, press 'm' key, and NERDTree menu will appear. Press 'o' then to open the file in the system default (editor, viewer, etc), or open the folder using the system's default file manager.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

