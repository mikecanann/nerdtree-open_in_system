" ============================================================================
" File:        open_in_system.vim
" Description: plugin for NERD Tree that provides an execute menu item, that
"              executes system default application for file or directory
" Maintainer:  Michael Canann <mikecanann@gmail.com>
" Last Change: 2017-0902
" ============================================================================
if exists("g:loaded_nerdtree_open_system_menuitem")
  finish
endif

let g:loaded_nerdtree_open_system_menuitem = 1

call NERDTreeAddMenuItem({
      \ 'text': '(o)pen file on system',
      \ 'shortcut': 'o',
      \ 'callback': 'NERDTreeExecute' })

function! NERDTreeExecute()
  let l:oldssl=&shellslash
  set noshellslash
  let treenode = g:NERDTreeFileNode.GetSelected()
  let path = treenode.path.str()

  if has("gui_running")
    let args = shellescape(path,1)." &"
  else
    let args = shellescape(path,1)." > /dev/null"
  end

  if has("unix") && executable("xdg-open")
    exe "silent !xdg-open ".args
    let ret= v:shell_error
  elseif has("unix") && executable("open")
    exe "silent !open ".args
    let ret= v:shell_error
  elseif has("win32") || has("win64")
    exe "silent !start explorer ".shellescape(path,1)
  end
  let &shellslash=l:oldssl
  redraw!
endfunction
